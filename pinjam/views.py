from django.shortcuts import render
from django.db import connection
from django.core.paginator import Paginator

def pinjam(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to komik;"
                       "SELECT kode, no_anggota, jumlah_komik, durasi_pinjam, total_biaya, tanggal_pinjam, tanggal_kembali, tanggal_dikembalikan "
                       "FROM transaksi_pinjam;")
        data_pinjam = dictfetchall(cursor)
    paginator = Paginator(data_pinjam, 10)
    page = request.GET.get('page')
    data = paginator.get_page(page)
    for d in data_pinjam:
        if(d['tanggal_dikembalikan'] <= d['tanggal_kembali']):
            d['status'] = 'Selesai'
        else:
            d['status'] = 'Belum'
    return render(request, "transaksi_pinjam.html", {'data_pinjam' : data})

def baca(request):
    return render(request, "transaksi_baca.html")

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
