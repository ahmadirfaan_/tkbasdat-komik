from django.contrib import admin
from django.urls import path
from .views import pinjam, baca

urlpatterns = [
    path('pinjam/', pinjam, name="pinjam"),
    path('baca/', baca, name="baca")
]