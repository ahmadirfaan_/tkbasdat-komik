from django.shortcuts import render
from django.db import connection

def index(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to komik;"
                       "SELECT id, nama, jumlah, tipe_produk "
                       "FROM produk "
                       "WHERE tipe_produk = 'Komik';")
        data_komik = dictfetchall(cursor)
    return render(request, "daftar_komik.html", {'data_komik' : data_komik})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
