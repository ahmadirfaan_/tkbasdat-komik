from django.apps import AppConfig


class KomikConfig(AppConfig):
    name = 'komik'
