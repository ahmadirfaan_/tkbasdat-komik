# Django Starter Template

#### Template ini dipersiapkan untuk di deploy di heroku

# Instalasi

- clone repository ini dengan cara
  ```
  git clone https://gitlab.com/anggardhanoano/django-starter.git
  ```
- masuk ke folder django-starter, nama folder dapat bebas diganti setelah di clone
  ```
  cd django-starter
  ```
- inisiasi git di folder yang kamu buat
  ```
  git init
  ```
- buat virtual environment
  ```
  python -m venv env
  ```
  atau
  ```
  python -m virtualenv env
  ```
- aktifkan virtual environment
  - untuk windows
    ```
    env/Scripts/activate.bat
    ```
  - untuk linux/mac
    ```
    source env/bin/activate
    ```
- install semua requirement yang ada, pastikan kamu berada di folder yang sama dengan file requirements.txt
  ```
  pip install -r requirements.txt
  ```
- cek apakah sudah bisa runserver
  ```
  python manage.py runserver
  ```

# Buat Repository kamu sendiri

### Untuk mempermudah kamu dalam mendeploy website ke heroku, maka buat lah akun gitlab terlebih dahulu jika belum punya

### Jika sudah memiliki akun atau sudah membuat akun, langsung saja buat repository baru, caranya gampang banget

    - lihat pojok kanan atas layar laptop kamu
    - cari tombol warna hijau yang tulisan nya "new project", tekan tombol tersebut
    - isi project name sesuai keiginan kamu, bebas pokoknya apa namanya
    - visibility level nya terserah kalian mau public atau private, kalo private artinya cuma orang orang tertentu aja yang bisa akses repo kalian
    - sisanya ga usah diisi dulu, dibiarkan saja
    - cuss!! langsung aja tekan tombol "Create project"

### oke kamu sekarang udah berhasil bikin repository kosong, saatnya kamu isi repo ini dengan website yang udah kamu bikin sebelumnya, ikutin step step nya

- masuk ke folder kalian menyimpan project django nya, untuk kasus ini misalnya folder "django-starter" atau jika kalian udah ganti namanya, masuk ke folder itu
- inisiasi git
  ```
  git init
  ```
- add remote origin
  ```
  git remote add origin <link repository kamu>
  ```
- add semua file kamu ke git
  ```
  git add .
  ```
- jangan lupa untuk di commit
  ```
  git commit -m "deploy pertama website sbf"
  ```
- dan, yang terakhir di push!!
  ```
  git push origin master
  ```
- saat kalian melakukan push, biasanya akan di tanyakan username gitlab kalian dan password nya, masukkan saja dan semua file akan ter push ke repo kalian.

# Deployment

### Supaya kamu bisa mengakses website kamu dan bisa pamer ke temen temen, berarti apa yang kita bikin harus di deploy, nah caranya gimana si? cuss ikutin langkah langkah ini

### sebelumnya kalian harus bikin akun dulu di heroku, langsung aja buka https://www.heroku.com/ lalu sign up, setelah kalian berhasil sign up, saatnya kalian bikin app baru di heroku

### cara bikin app di heroku itu simple, tinggal tekan tombol new di kanan atas, pilih create new app, lalu tulis website kalian mau memiliki domain apa, misalnya "websiteku". setelah itu langsung aja create app.

### setelah kalian buat app di heroku, kalian balik lagi ke gitlab, buka repo kalian, lalu coba cari menu settings di sidebar kanan lalu pilih "CI/CD"

### setelah itu kalian cari menu "Variables" dan expand

### nah disini kalian akan disuruh masukin secret key kalian. fungsi dari secret key ini untuk menyambungkan antara app kalian di heroku dengan repo kalian di gitlab, secara garis besar gitu.

    1. masukin variable pertama dengan key nya HEROKU_APIKEY dan value nya bisa kalian dapet di account settings heroku kalian, cari aja bagian "API Key" dan copas aja ke kotak value

    2. masukin variable kedua dengan key nya yaitu HEROKU_APPNAME dan value nya adalah nama app heroku kalian, misalnya "websiteku"

    3. masukin variable ketiga dengan key HEROKU_APP_HOST dan value link url dari app heroku kalian, untuk kasus contoh ini adalah https://websiteku.herokuapp.com/

### oke setelah itu kalian coba ke sidebar menu dan cari menu CI/CD

### yang kalian lihat adalah daftar pipeline kalian. nah kemungkinan besar dan hampir pasti pipeline kalian ada warna merah nya atau gagal, jangan panik karena emang itu yang diharapkan.

### kalian tinggal pilih retry aja dengan cara tekan tombol retry di kanan pipeline kalian. tunggu sampe pipeline nya selesai. tanda pipeline selesai itu saat hasilnya sukses(hijau) atau gagal(merah).

## Kalau ada error atau bingung, langsung tanya aja yaa, semangat guyss!!! :)

##### Created by Anggardha Febriano

##### Feel Free to share this repo if you think this template helpful!!
