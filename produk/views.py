from django.shortcuts import render
from django.db import connection
from django.core.paginator import Paginator

def index(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to komik;"
                       "SELECT id, nama, jumlah, tipe_produk "
                       "FROM produk;")
        data_produk = dictfetchall(cursor)
    paginator = Paginator(data_produk, 10)
    page = request.GET.get('page')
    data = paginator.get_page(page)
    return render(request, "daftar_produk.html", {'data_produk' : data})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
