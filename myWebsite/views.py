from django.shortcuts import render
from django.db import connection

def home(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to komik;"
                       "SELECT nama, id "
                       "FROM anggota;")
        data_anggota = dictfetchall(cursor)
    return render(request, "homepage.html", {'data_anggota' : data_anggota})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]