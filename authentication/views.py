from django.shortcuts import render
from .forms import LoginForm

# Create your views here.
def login_form(request):
    return  

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return form

def register(request):
    return render(request, 'register.html')