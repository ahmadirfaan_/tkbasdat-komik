from django import forms

class LoginForm(forms.Form):
    form_id = forms.CharField(label="Id", max_length=30)
    form_password = forms.CharField(widget=forms.PasswordInput())