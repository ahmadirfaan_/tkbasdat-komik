from django.contrib import admin
from django.urls import path
from .views import login, register
app_name = 'authentication'


urlpatterns = [
    path('login_form/', login, name="login_form"),
    path('login/', login, name="login"),
    path('register/', register, name="register"),
]