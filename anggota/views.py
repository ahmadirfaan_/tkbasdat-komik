from django.shortcuts import render
from django.db import connection
from django.core.paginator import Paginator

def index(request):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to komik")
        cursor.execute("SELECT no_anggota, id, nama, alamat, tanggal_lahir, no_hp FROM anggota")
        data_anggota = dictfetchall(cursor)
    paginator = Paginator(data_anggota, 10)
    page = request.GET.get('page')
    data = paginator.get_page(page)
    return render(request, "daftar_anggota.html", {'data_anggota' : data})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]